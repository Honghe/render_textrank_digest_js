import React, {Component} from 'react';
import './App.css';
import ReactDOM from "react-dom";
import json_list from './corpus_content';

/**
 * remove all whitespace except newline
 * @param text
 * @returns {string | void | *}
 */
function replaceSpace(text){
    let content = text.replace(/ /g, '');
    console.log(content);
    return(content);
}

class Card extends Component {
    render() {
        return (
            <div className="card">
                <div className="title clearfix">
                    <div className="float rowName">主题</div>
                    <div className="float rowContent">{replaceSpace(this.props.value.title)}</div>
                </div>
                <div className="origin clearfix">
                    <div className="float rowName">原文</div>
                    <div className="float rowContent">{replaceSpace(this.props.value.origin)}</div>
                </div>
                <div className="keyword clearfix">
                    <div className="float rowName">关键词</div>
                    <div className="float rowContent">
                        <ul style={{ margin: 0, padding: 0}}>{
                            this.props.value.keyword.slice(0,3).map(function (item, i) {
                                // li inline
                                return <li key={i} style={{display: 'inline', padding: '0 10px 0 0'}}>{item}</li>
                            })}
                        </ul>
                    </div>
                </div>
                <div className="digest clearfix">
                    <div className="float rowName">摘要</div>
                    <div className="float rowContent">{replaceSpace(this.props.value.digest.sentence)}</div>
                </div>
                <div style={{clear: 'both'}}></div>
            </div>
        );
    }
}

let container = document.querySelector("#container");

json_list.forEach(function (value, index) {
    let d = document.createElement("div");
    container.append(d);
    ReactDOM.render(<Card value={value}/>, d);
});

class App extends Component {
    render() {
        return (<div></div>)
    }
}

export default App;
